# Kafka Getting Going

```
docker-compose up -d
```

Visit http://localhost:3030/ for interface

Or download Conduktor: https://www.conduktor.io/download/ (free for dev)

### Python

```
cd python
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

You can run the `producer` and `consumer` separately:

```
python producer.py
```

```
python consumer.py
```

### Node

```
npm install
```

You can run `consumer.js` and `producer.js`.

# Links

https://github.com/lensesio/fast-data-dev
https://medium.com/@achilleus/getting-started-with-kafka-e016760eda3d

```
docker run --rm -it -p 2181:2181 -p 3030:3030 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 9092:9092 -e ADV_HOST=127.0.0.1 landoop/fast-data-dev
```
