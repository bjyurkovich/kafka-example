
from kafka import KafkaConsumer
from random import random
import json

# Start and set deserializer
# https://kafka-python.readthedocs.io/en/master/apidoc/KafkaConsumer.html
consumer = KafkaConsumer(
    str(random()*10000), bootstrap_servers='localhost:9092', value_deserializer=lambda m: json.loads(m.decode('ascii')))
consumer.subscribe(topics=('foobar'))
print(consumer.bootstrap_connected())

# Consume
for message in consumer:
    print(message)
    print(message.value)
