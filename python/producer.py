from kafka import KafkaProducer
import time
import random
import json

# Start and set serializer to json
# https://kafka-python.readthedocs.io/en/master/apidoc/KafkaProducer.html
producer = KafkaProducer(bootstrap_servers='localhost:9092',
                         value_serializer=lambda m: json.dumps(m).encode('ascii'))

# Just do some shit on topic foobar
while True:
    for _ in range(100):
        producer.send('foobar', {"number": random.random()})
    time.sleep(1)
