const { Consumer, Producer, KeyedMessage, KafkaClient } = require("kafka-node");

const client = new KafkaClient();
const producer = new Producer(client);
let km = new KeyedMessage("key", "message");
let payloads = [
  {
    topic: "node-foobar",
    messages: JSON.stringify({ whatever: Math.random() }),
    partition: 0,
  },
];

producer.on("ready", function () {
  setInterval(() => {
    producer.send(payloads, function (err, data) {
      console.log(data);
    });
  }, 500);
});

producer.on("error", function (err) {});
