const { Consumer, KafkaClient } = require("kafka-node");

let client = new KafkaClient();

let consumer = new Consumer(client, [{ topic: "node-foobar", partition: 0 }], {
  autoCommit: false,
});

consumer.on("message", (message) => {
  console.log(message);
});
